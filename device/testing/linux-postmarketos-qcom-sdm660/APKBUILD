# Maintainer: Alexey Min <alexey.min@gmail.com>
# Kernel config based on: arch/arm64/configs/defconfig

_flavor="postmarketos-qcom-sdm660"
pkgname=linux-$_flavor
pkgver=5.10_rc6
pkgrel=4
pkgdesc="Close to mainline linux kernel for Qualcomm Snapdragon 660 SoC"
arch="aarch64"
url="https://kernel.org/"
license="GPL-2.0-only"
options="!strip !check !tracedeps
	pmb:cross-native
	pmb:kconfigcheck-nftables
	"
makedepends="bison findutils flex installkernel openssl-dev perl"

_repo="linux-postmarketos"
_commit="68c4e52cf35374e4b654f252b62accadad759ca5"

source="
	$_repo-$_commit.tar.bz2::https://gitlab.com/postmarketOS/$_repo/-/archive/$_commit/$_repo-$_commit.tar.bz2
	config-$_flavor.aarch64
"

_carch="arm64"

builddir="$srcdir/$_repo-$_commit"

prepare() {
	default_prepare
	cp -v "$srcdir/config-$_flavor.$CARCH" "$builddir"/.config
}

build() {
	unset LDFLAGS
	make ARCH="$_carch" CC="${CC:-gcc}" \
		KBUILD_BUILD_VERSION="$((pkgrel + 1 ))-$_flavor"
}

package() {
	# bootloader requires compressed kernel
	install -Dm644 "$builddir/arch/$_carch/boot/Image.gz" \
		"$pkgdir/boot/vmlinuz"

	install -D "$builddir/include/config/kernel.release" \
		"$pkgdir/usr/share/kernel/$_flavor/kernel.release"

	make modules_install dtbs_install \
		ARCH="$_carch" \
		INSTALL_MOD_PATH="$pkgdir" \
		INSTALL_MOD_STRIP=1 \
		INSTALL_DTBS_PATH="$pkgdir/usr/share/dtb"
}

sha512sums="
3252b8fc1e17697f77c87176c681280461e886cb97af6f27f848c6022d0b3aff63996c251611a3ab863b2799ce3ae00e7522cc6d8ba3756db3a09a671426560b  linux-postmarketos-68c4e52cf35374e4b654f252b62accadad759ca5.tar.bz2
adec9da2f39fdbe97f79611fe45848426ba773b3f80252fc53d34ceaa4afb403af2d9a1429bbe60f0b810ff1976257a464d0674d0f9dba4fa36bbc7a1ec39b64  config-postmarketos-qcom-sdm660.aarch64
"
